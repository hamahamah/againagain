﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;


namespace AgainAgainWrapper
{
    static class AgainAgainFunctions
    {
        const string AgainAgainDllName = "AgainAgainDll_Debug.dll";


        [DllImport(AgainAgainDllName, CallingConvention = CallingConvention.Cdecl)]
        public extern static unsafe void Start( IntPtr aWindowHandle );


        [DllImport(AgainAgainDllName, CallingConvention = CallingConvention.Cdecl)]
        public extern static unsafe bool Update();


        [DllImport( AgainAgainDllName, CallingConvention = CallingConvention.Cdecl )]
        public extern static unsafe void Destroy();


        [DllImport( AgainAgainDllName, CallingConvention = CallingConvention.Cdecl )]
        public extern static unsafe void SetPlayerMovement( int aMoveDirection, bool aMoveFlag );


        [DllImport( AgainAgainDllName, CallingConvention = CallingConvention.Cdecl )]
        public extern static unsafe void SetWindowSize( uint aWidth, uint aHeight );


        [DllImport( AgainAgainDllName, CallingConvention = CallingConvention.Cdecl )]
        public extern static unsafe void DisableInput();


        [DllImport( AgainAgainDllName, CallingConvention = CallingConvention.Cdecl )]
        public extern static unsafe void EnableInput();
    }
}
