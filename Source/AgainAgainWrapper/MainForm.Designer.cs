﻿namespace AgainAgainWrapper
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._gamePanel = new System.Windows.Forms.Panel();
            this._upButton = new System.Windows.Forms.Button();
            this._downButton = new System.Windows.Forms.Button();
            this._rightButton = new System.Windows.Forms.Button();
            this._leftButton = new System.Windows.Forms.Button();
            this._playerMovementPanel = new System.Windows.Forms.Panel();
            this._gameInputEnabled = new System.Windows.Forms.CheckBox();
            this._playerMovementPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // _gamePanel
            // 
            this._gamePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._gamePanel.Location = new System.Drawing.Point(13, 12);
            this._gamePanel.Name = "_gamePanel";
            this._gamePanel.Size = new System.Drawing.Size(640, 480);
            this._gamePanel.TabIndex = 0;
            // 
            // _upButton
            // 
            this._upButton.Location = new System.Drawing.Point(53, 8);
            this._upButton.Name = "_upButton";
            this._upButton.Size = new System.Drawing.Size(45, 23);
            this._upButton.TabIndex = 1;
            this._upButton.Tag = "0";
            this._upButton.Text = "Up";
            this._upButton.UseVisualStyleBackColor = true;
            this._upButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MovePlayerButtonDown);
            this._upButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MovePlayerButtonUp);
            // 
            // _downButton
            // 
            this._downButton.Location = new System.Drawing.Point(53, 68);
            this._downButton.Name = "_downButton";
            this._downButton.Size = new System.Drawing.Size(45, 23);
            this._downButton.TabIndex = 1;
            this._downButton.Tag = "2";
            this._downButton.Text = "Down";
            this._downButton.UseVisualStyleBackColor = true;
            this._downButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MovePlayerButtonDown);
            this._downButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MovePlayerButtonUp);
            // 
            // _rightButton
            // 
            this._rightButton.Location = new System.Drawing.Point(98, 37);
            this._rightButton.Name = "_rightButton";
            this._rightButton.Size = new System.Drawing.Size(45, 23);
            this._rightButton.TabIndex = 1;
            this._rightButton.Tag = "1";
            this._rightButton.Text = "Right";
            this._rightButton.UseVisualStyleBackColor = true;
            this._rightButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MovePlayerButtonDown);
            this._rightButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MovePlayerButtonUp);
            // 
            // _leftButton
            // 
            this._leftButton.Location = new System.Drawing.Point(8, 37);
            this._leftButton.Name = "_leftButton";
            this._leftButton.Size = new System.Drawing.Size(45, 23);
            this._leftButton.TabIndex = 1;
            this._leftButton.Tag = "3";
            this._leftButton.Text = "Left";
            this._leftButton.UseVisualStyleBackColor = true;
            this._leftButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MovePlayerButtonDown);
            this._leftButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MovePlayerButtonUp);
            // 
            // _playerMovementPanel
            // 
            this._playerMovementPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._playerMovementPanel.Controls.Add(this._rightButton);
            this._playerMovementPanel.Controls.Add(this._leftButton);
            this._playerMovementPanel.Controls.Add(this._upButton);
            this._playerMovementPanel.Controls.Add(this._downButton);
            this._playerMovementPanel.Location = new System.Drawing.Point(659, 12);
            this._playerMovementPanel.Name = "_playerMovementPanel";
            this._playerMovementPanel.Size = new System.Drawing.Size(152, 100);
            this._playerMovementPanel.TabIndex = 2;
            // 
            // _gameInputEnabled
            // 
            this._gameInputEnabled.AutoSize = true;
            this._gameInputEnabled.Location = new System.Drawing.Point(675, 119);
            this._gameInputEnabled.Name = "_gameInputEnabled";
            this._gameInputEnabled.Size = new System.Drawing.Size(121, 17);
            this._gameInputEnabled.TabIndex = 3;
            this._gameInputEnabled.Text = "Game input enabled";
            this._gameInputEnabled.UseVisualStyleBackColor = true;
            this._gameInputEnabled.CheckedChanged += new System.EventHandler(this._gameInputEnabled_CheckedChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(823, 504);
            this.Controls.Add(this._gameInputEnabled);
            this.Controls.Add(this._playerMovementPanel);
            this.Controls.Add(this._gamePanel);
            this.MinimumSize = new System.Drawing.Size(839, 543);
            this.Name = "MainForm";
            this.Text = "AgainAgain Wrapper";
            this.ResizeEnd += new System.EventHandler(this.MainForm_ResizeEnd);
            this._playerMovementPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel _gamePanel;
        private System.Windows.Forms.Button _upButton;
        private System.Windows.Forms.Button _downButton;
        private System.Windows.Forms.Button _rightButton;
        private System.Windows.Forms.Button _leftButton;
        private System.Windows.Forms.Panel _playerMovementPanel;
        private System.Windows.Forms.CheckBox _gameInputEnabled;
    }
}

