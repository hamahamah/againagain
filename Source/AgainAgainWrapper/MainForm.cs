﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AgainAgainWrapper
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            IntPtr windowHandle = _gamePanel.Handle;

            Application.Idle += Update;
            _gamePanel.Invalidate();

            AgainAgainFunctions.Start( windowHandle );

            FormClosed += ( s, e ) => AgainAgainFunctions.Destroy();
        }


        void Update( object Sender, EventArgs e )
        {
            bool keepGoing = AgainAgainFunctions.Update();
            this.Invalidate();
        }


        private void MovePlayerButtonDown( object sender, MouseEventArgs e )
        {
            Button senderButton = (Button)sender;

            AgainAgainFunctions.SetPlayerMovement( Convert.ToInt32( senderButton.Tag ), true );
        }


        private void MovePlayerButtonUp( object sender, MouseEventArgs e )
        {
            Button senderButton = (Button)sender;

            AgainAgainFunctions.SetPlayerMovement( Convert.ToInt32( senderButton.Tag ), false );
        }


        private void MainForm_ResizeEnd( object sender, EventArgs e )
        {
            AgainAgainFunctions.SetWindowSize( Convert.ToUInt32( _gamePanel.Width ), Convert.ToUInt32( _gamePanel.Height ) );
        }


        private void _gameInputEnabled_CheckedChanged( object sender, EventArgs e )
        {
            CheckBox senderCheckbox = (CheckBox)sender;

            if( senderCheckbox.Checked )
            {
                AgainAgainFunctions.EnableInput();
                _playerMovementPanel.Enabled = false;
            }
            else
            {
                AgainAgainFunctions.DisableInput();
                _playerMovementPanel.Enabled = true;
            }
        }
    }
}
