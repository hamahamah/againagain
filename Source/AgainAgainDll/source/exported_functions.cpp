#include "stdafx.h"
#include "exported_functions.h"

#include <againagain/application_settings.h>
#include <againagain/application.h>


extern "C" AGAINAGAIN_DLL void Start( int *aWindowHandle )
{
    mhm::ApplicationSettings appSettings = mhm::ApplicationSettings::CreateFromFile( "settings.conf" );
    appSettings.windowCaption = "Again again!!";

    mhm::Application::Create( appSettings, (HWND)aWindowHandle );
}


extern "C" AGAINAGAIN_DLL bool Update( void )
{
    return mhm::Application::Instance()->update();
}


extern "C" AGAINAGAIN_DLL void Destroy( void )
{
    mhm::Application::Destroy();
}


extern "C" AGAINAGAIN_DLL void SetPlayerMovement( const int aDirection, const bool aMoveFlag )
{
    mhm::Application::Instance()->setPlayerMovement( static_cast<mhm::PlayerMovementDirection>( aDirection ), aMoveFlag );
}


extern "C" AGAINAGAIN_DLL void SetWindowSize( const unsigned int aWidth, const unsigned int aHeight )
{
    mhm::Application::Instance()->setWindowSize( aWidth, aHeight );
}


extern "C" AGAINAGAIN_DLL void DisableInput( void )
{
    mhm::Application::Instance()->disableInput();
}


extern "C" AGAINAGAIN_DLL void EnableInput( void )
{
    mhm::Application::Instance()->enableInput();
}
