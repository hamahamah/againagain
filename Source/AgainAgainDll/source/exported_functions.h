#pragma once


extern "C" AGAINAGAIN_DLL void Start( int *aWindowHandle );
extern "C" AGAINAGAIN_DLL bool Update( void );
extern "C" AGAINAGAIN_DLL void Destroy( void );
extern "C" AGAINAGAIN_DLL void SetPlayerMovement( const int aDirection, const bool aMoveFlag );
extern "C" AGAINAGAIN_DLL void SetWindowSize( const unsigned int aWidth, const unsigned int aHeight );
extern "C" AGAINAGAIN_DLL void DisableInput( void );
extern "C" AGAINAGAIN_DLL void EnableInput( void );
