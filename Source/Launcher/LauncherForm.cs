﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Diagnostics;
using System.IO;


namespace Launcher
{
    public partial class LauncherForm : Form
    {
        private int _launchTimerTicks = 2;

        public LauncherForm()
        {
            InitializeComponent();
        }


        private void LauncherForm_Load( object sender, EventArgs e )
        {
            int resolutionSelectorIndex = Properties.Settings.Default.SelectedResolution;
            if( resolutionSelectorIndex > _resolutionSelector.Items.Count )
            {
                resolutionSelectorIndex = -1;
            }
            _resolutionSelector.SelectedIndex = resolutionSelectorIndex;

            int antiAliasSelectorIndex = Properties.Settings.Default.SelectedAntiAlias;
            if( antiAliasSelectorIndex > _antiAliasSelector.Items.Count )
            {
                antiAliasSelectorIndex = -1;
            }
            _antiAliasSelector.SelectedIndex    = antiAliasSelectorIndex;

            _fullscreenCheckbox.Checked         = Properties.Settings.Default.FullScreen;
            _skipLauncherCheckbox.Checked       = Properties.Settings.Default.SkipLauncher;

            if( Properties.Settings.Default.SkipLauncher && EverythingSet() )
            {
                StartLaunchTimer();
            }
        }


        private void _resolutionSelector_SelectionChanged( object sender, EventArgs e )
        {
            Properties.Settings.Default.SelectedResolution = ( (ComboBox)sender).SelectedIndex;
            Properties.Settings.Default.Save();
        }


        private void _antiAliasSelector_SelectedIndexChanged( object sender, EventArgs e )
        {
            Properties.Settings.Default.SelectedAntiAlias = ( (ComboBox)sender ).SelectedIndex;
            Properties.Settings.Default.Save();
        }


        private void _fullscreenCheckbox_CheckedChanged( object sender, EventArgs e )
        {
            Properties.Settings.Default.FullScreen = ( (CheckBox)sender ).Checked;
            Properties.Settings.Default.Save();
        }


        private void _startGameButton_Click( object sender, EventArgs e )
        {
            if( _launchTimer.Enabled )
            {
                CancelLaunchTimer();
            }
            else
            {
                LaunchGame();
            }
        }


        private void _skipLauncherCheckbox_CheckedChanged( object sender, EventArgs e )
        {
            if( _launchTimer.Enabled )
            {
                CancelLaunchTimer();
            }

            Properties.Settings.Default.SkipLauncher = ( (CheckBox)sender ).Checked;
            Properties.Settings.Default.Save();
        }


        private bool EverythingSet()
        {
            return ( _resolutionSelector.SelectedIndex >= 0 && _antiAliasSelector.SelectedIndex >= 0 );
        }


        private void StartLaunchTimer()
        {
            _settingsPanel.Enabled = false;
            _launchTimer.Enabled = true;

            _startGameButton.Text = "Abort Launch (" + _launchTimerTicks + ")";
        }


        private void CancelLaunchTimer()
        {
            _settingsPanel.Enabled = true;
            _launchTimer.Enabled = false;

            _startGameButton.Text = "Start Game";
        }


        private void LaunchGame()
        {
            if( !EverythingSet() )
            {
                MessageBox.Show( "Not all settings are filled in correctly.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error );
                return;
            }

            // Save settings 
            using( BinaryWriter writer = new BinaryWriter( File.Open( "settings.conf", FileMode.Create ) ) )
            {
                Form launcherForm = Application.OpenForms["LauncherForm"];
                // Write header
                //
                writer.Write( "AgainAgainConfig".ToCharArray() );

                // Write version
                //
                writer.Write( 1 );
                writer.Write( 0 );
                writer.Write( 1 );

                // Write data
                //
                writer.Write( _resolutionSelector.SelectedIndex );
                writer.Write( _antiAliasSelector.SelectedIndex );
                writer.Write( _fullscreenCheckbox.Checked );
            }

            Process.Start( "AgainAgain_Debug.exe" );
            Application.Exit();
        }


        private void _launchTimer_Tick( object sender, EventArgs e )
        {
            _launchTimerTicks--;

            _startGameButton.Text = "Abort Launch (" + _launchTimerTicks + ")";

            if( _launchTimerTicks <= 0 )
            {
                LaunchGame();
            }
        }
    }
}
