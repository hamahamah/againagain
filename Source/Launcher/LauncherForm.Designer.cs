﻿namespace Launcher
{
    partial class LauncherForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._startGameButton = new System.Windows.Forms.Button();
            this._resolutionSelector = new System.Windows.Forms.ComboBox();
            this._resolutionLabel = new System.Windows.Forms.Label();
            this._fullscreenCheckbox = new System.Windows.Forms.CheckBox();
            this._settingsPanel = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this._skipLauncherExplanation = new System.Windows.Forms.Label();
            this._skipLauncherCheckbox = new System.Windows.Forms.CheckBox();
            this._antiAliasSelector = new System.Windows.Forms.ComboBox();
            this._antiAliasLabel = new System.Windows.Forms.Label();
            this._launchTimer = new System.Windows.Forms.Timer(this.components);
            this._settingsPanel.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // _startGameButton
            // 
            this._startGameButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._startGameButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._startGameButton.Location = new System.Drawing.Point(12, 354);
            this._startGameButton.Name = "_startGameButton";
            this._startGameButton.Size = new System.Drawing.Size(260, 70);
            this._startGameButton.TabIndex = 0;
            this._startGameButton.Text = "Start Game";
            this._startGameButton.UseVisualStyleBackColor = true;
            this._startGameButton.Click += new System.EventHandler(this._startGameButton_Click);
            // 
            // _resolutionSelector
            // 
            this._resolutionSelector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._resolutionSelector.FormattingEnabled = true;
            this._resolutionSelector.Items.AddRange(new object[] {
            "640x480",
            "800x600",
            "1024x769"});
            this._resolutionSelector.Location = new System.Drawing.Point(3, 36);
            this._resolutionSelector.Name = "_resolutionSelector";
            this._resolutionSelector.Size = new System.Drawing.Size(249, 21);
            this._resolutionSelector.TabIndex = 1;
            this._resolutionSelector.SelectionChangeCommitted += new System.EventHandler(this._resolutionSelector_SelectionChanged);
            // 
            // _resolutionLabel
            // 
            this._resolutionLabel.AutoSize = true;
            this._resolutionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._resolutionLabel.Location = new System.Drawing.Point(3, 7);
            this._resolutionLabel.Name = "_resolutionLabel";
            this._resolutionLabel.Size = new System.Drawing.Size(115, 26);
            this._resolutionLabel.TabIndex = 2;
            this._resolutionLabel.Text = "Resolution";
            // 
            // _fullscreenCheckbox
            // 
            this._fullscreenCheckbox.AutoSize = true;
            this._fullscreenCheckbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._fullscreenCheckbox.Location = new System.Drawing.Point(57, 142);
            this._fullscreenCheckbox.Name = "_fullscreenCheckbox";
            this._fullscreenCheckbox.Size = new System.Drawing.Size(137, 30);
            this._fullscreenCheckbox.TabIndex = 3;
            this._fullscreenCheckbox.Text = "Full screen";
            this._fullscreenCheckbox.UseVisualStyleBackColor = true;
            this._fullscreenCheckbox.CheckedChanged += new System.EventHandler(this._fullscreenCheckbox_CheckedChanged);
            // 
            // _settingsPanel
            // 
            this._settingsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._settingsPanel.Controls.Add(this._fullscreenCheckbox);
            this._settingsPanel.Controls.Add(this._resolutionSelector);
            this._settingsPanel.Controls.Add(this._antiAliasSelector);
            this._settingsPanel.Controls.Add(this._resolutionLabel);
            this._settingsPanel.Controls.Add(this._antiAliasLabel);
            this._settingsPanel.Location = new System.Drawing.Point(12, 12);
            this._settingsPanel.Name = "_settingsPanel";
            this._settingsPanel.Size = new System.Drawing.Size(260, 179);
            this._settingsPanel.TabIndex = 5;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this._skipLauncherExplanation);
            this.panel3.Controls.Add(this._skipLauncherCheckbox);
            this.panel3.Location = new System.Drawing.Point(12, 270);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(260, 78);
            this.panel3.TabIndex = 5;
            // 
            // _skipLauncherExplanation
            // 
            this._skipLauncherExplanation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._skipLauncherExplanation.Location = new System.Drawing.Point(11, 10);
            this._skipLauncherExplanation.Name = "_skipLauncherExplanation";
            this._skipLauncherExplanation.Size = new System.Drawing.Size(232, 32);
            this._skipLauncherExplanation.TabIndex = 4;
            this._skipLauncherExplanation.Text = "You will have five seconds to skip launch if this is checked.";
            // 
            // _skipLauncherCheckbox
            // 
            this._skipLauncherCheckbox.AutoSize = true;
            this._skipLauncherCheckbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._skipLauncherCheckbox.Location = new System.Drawing.Point(42, 45);
            this._skipLauncherCheckbox.Name = "_skipLauncherCheckbox";
            this._skipLauncherCheckbox.Size = new System.Drawing.Size(170, 30);
            this._skipLauncherCheckbox.TabIndex = 3;
            this._skipLauncherCheckbox.Text = "Skip Launcher";
            this._skipLauncherCheckbox.UseVisualStyleBackColor = true;
            this._skipLauncherCheckbox.CheckedChanged += new System.EventHandler(this._skipLauncherCheckbox_CheckedChanged);
            // 
            // _antiAliasSelector
            // 
            this._antiAliasSelector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._antiAliasSelector.FormattingEnabled = true;
            this._antiAliasSelector.Items.AddRange(new object[] {
            "None",
            "1x",
            "2x",
            "4x",
            "8x"});
            this._antiAliasSelector.Location = new System.Drawing.Point(3, 99);
            this._antiAliasSelector.Name = "_antiAliasSelector";
            this._antiAliasSelector.Size = new System.Drawing.Size(249, 21);
            this._antiAliasSelector.TabIndex = 1;
            this._antiAliasSelector.SelectedIndexChanged += new System.EventHandler(this._antiAliasSelector_SelectedIndexChanged);
            this._antiAliasSelector.SelectionChangeCommitted += new System.EventHandler(this._resolutionSelector_SelectionChanged);
            // 
            // _antiAliasLabel
            // 
            this._antiAliasLabel.AutoSize = true;
            this._antiAliasLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._antiAliasLabel.Location = new System.Drawing.Point(3, 70);
            this._antiAliasLabel.Name = "_antiAliasLabel";
            this._antiAliasLabel.Size = new System.Drawing.Size(101, 26);
            this._antiAliasLabel.TabIndex = 2;
            this._antiAliasLabel.Text = "Anti alias";
            // 
            // _launchTimer
            // 
            this._launchTimer.Interval = 1000;
            this._launchTimer.Tick += new System.EventHandler(this._launchTimer_Tick);
            // 
            // LauncherForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 436);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this._settingsPanel);
            this.Controls.Add(this._startGameButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LauncherForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "AgainAgain Launcher";
            this.Load += new System.EventHandler(this.LauncherForm_Load);
            this._settingsPanel.ResumeLayout(false);
            this._settingsPanel.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _startGameButton;
        private System.Windows.Forms.ComboBox _resolutionSelector;
        private System.Windows.Forms.Label _resolutionLabel;
        private System.Windows.Forms.CheckBox _fullscreenCheckbox;
        private System.Windows.Forms.Panel _settingsPanel;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label _skipLauncherExplanation;
        private System.Windows.Forms.CheckBox _skipLauncherCheckbox;
        private System.Windows.Forms.ComboBox _antiAliasSelector;
        private System.Windows.Forms.Label _antiAliasLabel;
        private System.Windows.Forms.Timer _launchTimer;
    }
}

