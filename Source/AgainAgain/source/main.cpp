#include "stdafx.h"

#include <againagain/application_settings.h>
#include <againagain/application.h>


int main( const int /*argc*/, const char * /*argv*/[] )
{
    mhm::ApplicationSettings appSettings = mhm::ApplicationSettings::CreateFromFile( "settings.conf" );
    appSettings.windowCaption = "Again again!!";

    mhm::Application::Create( appSettings );
    mhm::Application *app = mhm::Application::Instance();
    app->start();

    while( app->update() );

    mhm::Application::Destroy();

    return 0;
}
