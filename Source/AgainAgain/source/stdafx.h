#pragma once


#include <queue>
#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <array>
#include <string>
#include <sstream>
#include <tuple>
#include <stack>
#include <unordered_map>
#include <vector>
#include <fstream>

// Windows
//
#include <Windows.h>

// SFML
//
#pragma warning( push, 3 )
#include <SFML/Graphics.hpp>
#pragma warning( pop )


// Utilities
//
#include <utilities/vector2.h>
#include <utilities/color.h>
#include <utilities/hashed_string.h>
#include <utilities/version.h>
