#pragma once


#include "types.h"


namespace mhm
{
    class SimulationEntity
    {
    public:     SimulationEntity( const EntityId &anId, const Vector2f &aStartPosition );

                // Accessors
                //
                const EntityId &id( void ) const{ return _id; }
                const Vector2f &position( void ) const { return _position; }
                void setPosition( const Vector2f &aPosition ) { _position = aPosition; }

    private:    EntityId    _id;
                Vector2f    _position;
    };
}
