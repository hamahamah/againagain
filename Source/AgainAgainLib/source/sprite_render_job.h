#pragma once


#include "i_render_job.h"


namespace mhm
{
    class SpriteRenderJob : public IRenderJob
    {
    public:     SpriteRenderJob( const sf::Sprite &aSprite, const sf::Vector2f &aPosition );

                virtual void draw( sf::RenderWindow &aWindow );

    private:    sf::Sprite _sprite;
    };
}
