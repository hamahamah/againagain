#pragma once


// Forward declarations
//
namespace mhm
{
    class TextureCache;
    typedef std::unique_ptr<TextureCache> TextureCacheUPtr;
}

namespace mhm
{
    class TextureCache
    {
    public:     static void Create( void );
                static TextureCacheUPtr &Instance( void );

                const sf::Texture &getTexture( const HashedString &aTextureId ) const;

    private:    typedef std::unordered_map<HashedString, sf::Texture> TextureMap;

                static TextureCacheUPtr _Instance;

                TextureMap _textures;
    };
}
