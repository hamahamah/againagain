#pragma once


// Forward declarations
//
namespace sf
{
    class Texture;
}


namespace mhm
{
    class ResourceManager
    {
    public:     ResourceManager( void );
                ~ResourceManager( void );

                void loadResources( std::istream &anInputStream );

                sf::Texture &getTexture( const HashedString &aTextureId );

    private:    typedef std::unordered_map<HashedString, sf::Texture> TextureMap;

                TextureMap _textures;
    };
}
