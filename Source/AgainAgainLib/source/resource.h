#pragma once


#include "types.h"


namespace mhm
{
    class Resource
    {
    public:     virtual const ResourceId &id( void ) const = 0;
    };
}
