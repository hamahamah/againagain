#pragma once


#include "types.h"
#include "simulation_entity.h"


// Forward declarations
//
namespace mhm
{
    class SimulationEntity;
    typedef std::unique_ptr<SimulationEntity> SimulationEntityUPtr;
}


namespace mhm
{
    class Simulation
    {
    public:     typedef std::function<void( const SimulationEntityUPtr & )> EntityVisitor;

                Simulation( void );

                void step( const sf::Time &aGameTime );
                void addEntity( const EntityId &anId, const Vector2f &aStartPosition );
                void visitEntities( EntityVisitor aVisitor ) const;
                void moveEntity( const EntityId &anId, const Vector2f &aMoveVector );

                // Accessors
                //
                const sf::Time &simulationTime( void ) const { return _simulationTime; }

    private:    typedef std::unordered_map<EntityId, SimulationEntityUPtr> EntityMap;

                sf::Time        _lastTime,
                                _timeAccumulator,
                                _simulationTime;
                const sf::Time  _stepSize;
                float           _timeScale;
                EntityMap       _entities;

                // Remove stuff that shouldn't work
                //
                Simulation &operator =( const Simulation &other ) = delete;
                Simulation( const Simulation &other ) = delete;
    };
}
