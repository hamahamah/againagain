#pragma once


#include "i_drawable.h"


namespace mhm
{
    class CircleDrawable : public IDrawable
    {
    public:     CircleDrawable( const float aRadius, const Color &aColor );

                virtual IRenderJobUPtr createRenderJob( const Vector2f &aPosition );

                static void RegisterFactory( void );

                static const HashedString Type;

    private:    sf::CircleShape _circle;
    };
}
