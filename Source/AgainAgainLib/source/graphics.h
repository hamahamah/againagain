#pragma once


// Forward declarations
//
namespace mhm
{
    class Simulation;
    typedef const std::unique_ptr<Simulation> SimulationCUPtr;
}


namespace mhm
{
    class Graphics
    {
    public:     struct Settings
                {
                    Vector2i windowSize;
                    bool fullscreen;
                    int antialiasLevel;
                    std::string windowCaption;
                };


                Graphics( const Settings &someSettings );
                Graphics( const Settings &someSettings, const HWND aWindowHandle );
                ~Graphics( void );

                void update( void );
                void render( const SimulationCUPtr &aSimulation );
                void setWindowSize( const unsigned int aWidth, const unsigned int aHeight );
                void finish( void );
                void loadDrawables( std::istream &anInputStream );

                // Accessors
                //
                bool isWindowOpen( void ) const;

    private:    class impl;
                std::unique_ptr<impl> _impl;
    };

    typedef const std::unique_ptr<Graphics> GraphicsCUPtr;
}
