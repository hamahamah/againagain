#include "stdafx.h"
#include "simulation.h"

#include "simulation_entity.h"


namespace mhm
{
    Simulation::Simulation( void )
    : _stepSize( sf::seconds( 1.0f / 60.0f ) )
    , _timeScale( 1.0f )
    {

    }


    void Simulation::step( const sf::Time &aGameTime )
    {
        sf::Time deltaT = aGameTime - _lastTime;
        _lastTime = aGameTime;
        _simulationTime += deltaT * _timeScale;

        // Here we update the simulation
    }


    void Simulation::addEntity( const EntityId &anId, const Vector2f &aStartPosition )
    {
        _entities.insert( EntityMap::value_type( anId, std::make_unique<SimulationEntity>( anId, aStartPosition ) ) );
    }


    void Simulation::visitEntities( EntityVisitor aVisitor ) const
    {
        for( auto &&currentEntity : _entities )
        {
            aVisitor( currentEntity.second );
        }
    }


    void Simulation::moveEntity( const EntityId &anId, const Vector2f &aMoveVector )
    {
        auto &&foundEntityIterator = _entities.find( anId );

        if( foundEntityIterator == _entities.end() )
        {
            throw new std::exception( "Trying to move entity that doesn't exist." );
        }

        SimulationEntityUPtr &foundEntity = foundEntityIterator->second;
        foundEntity->setPosition( foundEntity->position() + aMoveVector );
    }
}
