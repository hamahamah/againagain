#include "stdafx.h"
#include "circle_drawable.h"

#include "circle_render_job.h"
#include "sfml_conversions.h"
#include "drawable_factory.h"


namespace mhm
{
    const HashedString CircleDrawable::Type( "Circle" );


    CircleDrawable::CircleDrawable( const float aRadius, const Color &aColor )
    : _circle( aRadius )
    {
        _circle.setFillColor( ToRenderer( aColor ) );
    }


    IRenderJobUPtr CircleDrawable::createRenderJob( const Vector2f &aPosition )
    {
        return std::make_unique<CircleRenderJob>( _circle, ToRenderer( aPosition ) );
    }


    class CircleDrawableFactory : public DrawableFactory
    {
    public:     CircleDrawableFactory( void );

    private:    virtual IDrawableUPtr makeDrawable( std::istream &anInputStream );
                virtual const HashedString &type( void ) const;
    };


    CircleDrawableFactory::CircleDrawableFactory( void )
    {}


    IDrawableUPtr CircleDrawableFactory::makeDrawable( std::istream &anInputStream )
    {
        float radius;
        anInputStream.read( reinterpret_cast<char *>( &radius ), sizeof( radius ) );
        Color color;
        anInputStream >> color;

        return std::make_unique<CircleDrawable>( radius, color );
    }


    const HashedString &CircleDrawableFactory::type( void ) const
    {
        return CircleDrawable::Type;
    }


    void CircleDrawable::RegisterFactory( void )
    {
        DrawableFactoryUPtr circleDrawableFactory = std::make_unique<CircleDrawableFactory>();

        DrawableFactory::Register( circleDrawableFactory );
    }
}
