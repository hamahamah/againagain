#pragma once


// Forward declarations
//
namespace mhm
{
    class IDrawable;
    typedef std::unique_ptr<IDrawable> IDrawableUPtr;

    class DrawableFactory;
    typedef std::unique_ptr<DrawableFactory> DrawableFactoryUPtr;
}


namespace mhm
{
    class DrawableFactory
    {
    public:     static void Register( DrawableFactoryUPtr &aFactory );
                static IDrawableUPtr CreateDrawable( std::istream &anInputStream );

    private:    virtual IDrawableUPtr makeDrawable( std::istream &anInputStream ) = 0;
                virtual const HashedString &type( void ) const = 0;

                typedef std::unordered_map<HashedString, DrawableFactoryUPtr> FactoryRegistry;

                static FactoryRegistry _Factories;
    };
}
