#pragma once


namespace mhm
{
    class IRenderJob
    {
    public:     virtual void draw( sf::RenderWindow &aWindow ) = 0;
    };
}
