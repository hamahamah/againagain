#pragma once


#include "i_drawable.h"


namespace mhm
{
    class SpriteDrawable : public IDrawable
    {
    public:     SpriteDrawable( const sf::Texture &aTexture );
                
                virtual IRenderJobUPtr createRenderJob( const Vector2f &aPosition );

                static void RegisterFactory( void );

                static const HashedString Type;

    private:    sf::Sprite _sprite;
    };
}
