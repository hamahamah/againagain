#include "stdafx.h"
#include "resource_manager.h"


namespace mhm
{
    static const HashedString TextureId( "Texture" );

    std::istream &operator >>( std::istream &anInputStream, sf::Texture &aTexture )
    {
        // Load image

        // Set smoothness
        
        // Set repeating
    }


    ResourceManager::ResourceManager( void )
    {

    }


    ResourceManager::~ResourceManager( void )
    {

    }


    void ResourceManager::loadResources( std::istream &anInputStream )
    {
        HashedString resourceType;
        anInputStream >> resourceType;

        if( resourceType == TextureId )
        {
            HashedString textureId;
            anInputStream >> textureId;

            sf::Texture texture;
            anInputStream >> texture;

            _textures[textureId] = texture;
        }
    }


    sf::Texture &ResourceManager::getTexture( const HashedString &aTextureId )
    {
        auto &&foundTextureIterator = _textures.find( aTextureId );

        if( foundTextureIterator != _textures.end() )
        {
            return foundTextureIterator->second;
        }
    }
}
