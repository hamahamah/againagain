#include "stdafx.h"
#include "application_settings.h"


namespace mhm
{
    const std::array<int, ApplicationSettings::AntialiasLevel::NumAntialiasLevels> ApplicationSettings::AntialiasLevels =
    {
        0,
        1,
        2,
        4,
        8
    };


    const std::array<Vector2i, ApplicationSettings::Resolution::NumResolutions> ApplicationSettings::Resolutions =
    {
        Vector2i( 640, 480 ),
        Vector2i( 800, 600 ),
        Vector2i( 1024, 768 )
    };


    const std::string ApplicationSettings::TypeHeader = "AgainAgainConfig";
    const Version ApplicationSettings::SupportedVersion = { 1, 0, 0 };


    ApplicationSettings ApplicationSettings::CreateFromFile( const std::string &aSettingsFilename )
    {
        ApplicationSettings settings;

        std::ifstream configFile;
        configFile.open( aSettingsFilename, std::ios::in | std::ios::binary );

        // Skip the header string
        //
        char typeHeader[17]{};
        configFile.read( reinterpret_cast<char *>( &typeHeader ), 16 );

        if( TypeHeader != typeHeader )
        {
            throw new std::exception( "File is not a config file." );
        }
 
        Version version;
        configFile >> version;

        if( version.major > SupportedVersion.major )
        {
            throw new std::exception( "Unsupported version of config file." );
        }

        configFile >> settings;

        return settings;
    }


    std::istream &operator >>( std::istream &anInputFile, ApplicationSettings &someSettings )
    {
        ApplicationSettings::Resolution resolution;
        anInputFile.read( reinterpret_cast<char *>( &resolution ), sizeof( resolution ) );
        someSettings.windowSize = ApplicationSettings::Resolutions[resolution];

        ApplicationSettings::AntialiasLevel aaLevel;
        anInputFile.read( reinterpret_cast<char *>( &aaLevel ), sizeof( aaLevel ) );
        someSettings.antialiasLevel = ApplicationSettings::AntialiasLevels[aaLevel];

        anInputFile.read( reinterpret_cast<char *>( &someSettings.fullscreen ), sizeof( someSettings.fullscreen ) );

        return anInputFile;
    }
}