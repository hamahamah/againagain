#include "stdafx.h"
#include "application.h"

#include "application_settings.h"
#include "graphics.h"
#include "simulation.h"
#include "circle_drawable.h"
#include "graphics.h"
#include "sprite_drawable.h"


namespace mhm
{
    ApplicationUPtr Application::_Instance;


    class Application::impl
    {
    public:     impl( const ApplicationSettings &someSettings );
                impl( const ApplicationSettings &someSettings, const HWND aWindowHandle );
                ~impl( void );

                void start( void );
                bool update( void );
                void setPlayerMovement( const PlayerMovementDirection aDirection, const bool aMoveFlag );
                void setWindowSize( const unsigned int aWidth, const unsigned int aHeight );
                void disableInput( void );
                void enableInput( void );

    private:    void handleInput( void );
                void updatePlayerMovement( const sf::Time &anApplicationTime );
                std::istream &createMockStream( void );

                sf::Clock   _applicationClock;

                // Subsystems
                //
                GraphicsCUPtr   _graphics;
                SimulationCUPtr _simulation;

                // Remove stuff that shouldn't work
                //
                impl &operator =( const Application &other ) = delete;
                impl( const Application &other ) = delete;

                std::array<int, PlayerMovementDirection::NumDirections> _playerMovement;
                bool _shouldHandleInput;
    };


    // This doesn't really do anything at the moment but the compiler needs it because of the std::unique_ptr's
    //
    Application::impl::~impl( void )
    {
        _graphics->finish();
    }


    void Application::impl::start( void )
    {
        _graphics->loadDrawables( createMockStream() );
    }


    bool Application::impl::update( void )
    {
        // Check if it's time to pack up and leave.
        //
        if( !_graphics->isWindowOpen() )
        {
            return false;
        }

        // Poll the window events or SFML gets cross
        //
        _graphics->update();

        // Update timers
        //
        sf::Time currentApplicationTime = _applicationClock.getElapsedTime();

        // Check input
        //
        if( _shouldHandleInput )
        {
            handleInput();
        }

        updatePlayerMovement( currentApplicationTime );

        // Update simulation
        //
        _simulation->step( currentApplicationTime );

        // Render
        //
        _graphics->render( _simulation );

        return true;
    }


    void Application::impl::setPlayerMovement( const PlayerMovementDirection aDirection, const bool aMoveFlag )
    {
        _playerMovement[aDirection] = aMoveFlag;
    }


    void Application::impl::setWindowSize( const unsigned int aWidth, const unsigned int aHeight )
    {
        _graphics->setWindowSize( aWidth, aHeight );
    }


    void Application::impl::disableInput( void )
    {
        _shouldHandleInput = false;
    }


    void Application::impl::enableInput( void )
    {
        _shouldHandleInput = true;
    }


    Application::impl::impl( const ApplicationSettings &someSettings )
    : _graphics( std::make_unique<Graphics>( Graphics::Settings{ someSettings.windowSize, someSettings.fullscreen, someSettings.antialiasLevel, someSettings.windowCaption } ) )
    , _simulation( std::make_unique<Simulation>() )
    , _shouldHandleInput( true )
    {
        _simulation->addEntity( std::string( "Player" ), Vector2f( 300.0f, 100.0f ) );
    }


    Application::impl::impl( const ApplicationSettings &someSettings, const HWND aWindowHandle )
    : _graphics( std::make_unique<Graphics>( Graphics::Settings{ someSettings.windowSize, someSettings.fullscreen, someSettings.antialiasLevel, someSettings.windowCaption }, aWindowHandle ) )
    , _simulation( std::make_unique<Simulation>() )
    , _playerMovement{}
    , _shouldHandleInput( false )
    {
        _simulation->addEntity( std::string( "Player" ), Vector2f( 300.0f, 100.0f ) );
    }


    void Application::impl::handleInput( void )
    {
        if( sf::Keyboard::isKeyPressed( sf::Keyboard::W ) )
        {
            _playerMovement[PlayerMovementDirection::Up] = true;
        }
        else
        {
            _playerMovement[PlayerMovementDirection::Up] = false;
        }

        if( sf::Keyboard::isKeyPressed( sf::Keyboard::D ) )
        {
            _playerMovement[PlayerMovementDirection::Right] = true;
        }
        else
        {
            _playerMovement[PlayerMovementDirection::Right] = false;
        }

        if( sf::Keyboard::isKeyPressed( sf::Keyboard::S ) )
        {
            _playerMovement[PlayerMovementDirection::Down] = true;
        }
        else
        {
            _playerMovement[PlayerMovementDirection::Down] = false;
        }

        if( sf::Keyboard::isKeyPressed( sf::Keyboard::A ) )
        {
            _playerMovement[PlayerMovementDirection::Left] = true;
        }
        else
        {
            _playerMovement[PlayerMovementDirection::Left] = false;
        }
    }


    void Application::impl::updatePlayerMovement( const sf::Time &anApplicationTime )
    {
        Vector2f playerMovement = Vector2f::Zero;

        if( _playerMovement[PlayerMovementDirection::Up] )
        {
            playerMovement -= Vector2f::UnitY;
        }

        if( _playerMovement[PlayerMovementDirection::Right] )
        {
            playerMovement += Vector2f::UnitX;
        }

        if( _playerMovement[PlayerMovementDirection::Down] )
        {
            playerMovement += Vector2f::UnitY;
        }

        if( _playerMovement[PlayerMovementDirection::Left] )
        {
            playerMovement -= Vector2f::UnitX;
        }

        static sf::Time lastTime;
        sf::Time deltaT = anApplicationTime - lastTime;
        lastTime = anApplicationTime;

        if( playerMovement != Vector2f::Zero )
        {
            playerMovement.normalize();
            playerMovement *= 100.0f * deltaT.asSeconds();
            _simulation->moveEntity( std::string( "Player" ), playerMovement );
        }
    }


    std::istream &Application::impl::createMockStream( void )
    {
        static std::stringstream mockStream;

        // Player thing
        //
        EntityId playerId( "Player" );
        HashedString::HashType playerIdHash = playerId.hash;
        mockStream.write( reinterpret_cast<char *>( &playerIdHash ), sizeof( playerIdHash ) );

        HashedString::HashType spriteTypeHash = SpriteDrawable::Type.hash;
        mockStream.write( reinterpret_cast<char *>( &spriteTypeHash ), sizeof( spriteTypeHash ) );

        EntityId spriteTextureId( "pirate.png" );
        HashedString::HashType spriteTextureIdHash = spriteTextureId.hash;
        mockStream.write( reinterpret_cast<char *>( &spriteTextureIdHash ), sizeof( spriteTextureIdHash ) );

        // Other thing
        //
        EntityId otherId( "Other" );
        HashedString::HashType otherIdHash = otherId.hash;
        mockStream.write( reinterpret_cast<char *>( &otherIdHash ), sizeof( otherIdHash ) );

        HashedString::HashType circleTypeHash = CircleDrawable::Type.hash;
        mockStream.write( reinterpret_cast<char *>( &circleTypeHash ), sizeof( circleTypeHash ) );

        float radius = 32.0f;
        mockStream.write( reinterpret_cast<char *>( &radius ), sizeof( radius ) );

        Color color = Color::Cyan;
        mockStream << color;

        return mockStream;
    }


    void Application::Create( const ApplicationSettings &someSettings )
    {
        if( _Instance )
        {
            throw new std::exception( "Can't create application twice." );
        }

        _Instance = ApplicationUPtr( new Application( someSettings ) );
    }


    void Application::Create( const ApplicationSettings &someSettings, const HWND aWindowHandle )
    {
        if( _Instance )
        {
            throw new std::exception( "Can't create application twice." );
        }

        _Instance = ApplicationUPtr( new Application( someSettings, aWindowHandle ) );
    }


    Application *Application::Instance( void )
    {
        return _Instance.get();
    }


    void Application::Destroy( void )
    {
        _Instance.reset( nullptr );
    }


    Application::~Application( void )
    {

    }


    void Application::start( void )
    {
        _impl->start();
    }


    bool Application::update( void )
    {
        return _impl->update();
    }


    void Application::setPlayerMovement( const PlayerMovementDirection aDirection, const bool aMoveFlag )
    {
        _impl->setPlayerMovement( aDirection, aMoveFlag );
    }


    void Application::setWindowSize( const unsigned int aWidth, const unsigned int aHeight )
    {
        _impl->setWindowSize( aWidth, aHeight );
    }


    void Application::disableInput( void )
    {
        _impl->disableInput();
    }


    void Application::enableInput( void )
    {
        _impl->enableInput();
    }


    Application::Application( const ApplicationSettings &someSettings )
    : _impl( std::make_unique<impl>( someSettings ) )
    {
    }


    Application::Application( const ApplicationSettings &someSettings, const HWND aWindowHandle )
    : _impl( std::make_unique<impl>( someSettings, aWindowHandle ) )
    {
    }
}
