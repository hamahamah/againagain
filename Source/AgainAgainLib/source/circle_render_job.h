#pragma once


#include "i_render_job.h"


namespace mhm
{
    class CircleRenderJob : public IRenderJob
    {
    public:     CircleRenderJob( const sf::CircleShape &aShape, const sf::Vector2f &aPosition );

                void draw( sf::RenderWindow &aWindow );

    private:    sf::CircleShape _circle;
    };
}