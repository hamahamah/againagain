#include "stdafx.h"
#include "sfml_conversions.h"


namespace mhm
{
    sf::Color ToRenderer( const Color &aColor )
    {
        return sf::Color(
            static_cast<sf::Uint8>( 255 * aColor.r ),
            static_cast<sf::Uint8>( 255 * aColor.g ),
            static_cast<sf::Uint8>( 255 * aColor.b ),
            static_cast<sf::Uint8>( 255 * aColor.a ) );
    }


    sf::Vector2f ToRenderer( const Vector2f &aVector )
    {
        return sf::Vector2f( aVector.x, aVector.y );
    }
}
