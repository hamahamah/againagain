#include "stdafx.h"
#include "graphics.h"

#include "simulation.h"
#include "simulation_entity.h"
#include "i_drawable.h"
#include "circle_drawable.h"
#include "i_render_job.h"
#include "drawable_factory.h"
#include "sprite_drawable.h"


namespace mhm
{
    class Graphics::impl
    {
    public:     impl( const Settings &someSettings );
                impl( const Settings &someSettings, const HWND aWindowHandle );
                ~impl( void );

                void update( void );
                void render( const SimulationCUPtr &aSimulation );
                void setWindowSize( const unsigned int aWidth, const unsigned int aHeight );
                void finish( void );
                void loadDrawables( std::istream &anInputStream );

                // Accessors
                //
                bool isWindowOpen( void ) const;

    private:    typedef std::unordered_map<EntityId, IDrawableUPtr> DrawableMap;
                typedef std::vector<IRenderJobUPtr> RenderQueue;

                sf::ContextSettings createContextSettings( const int anAntialiasLevel );

                void init( void );
                void renderInThread( void );
                void renderQueue( const RenderQueue &aRenderQueue );
                bool hasRenderThread( void );

                sf::RenderWindow        _window;
                DrawableMap             _drawables;
                RenderQueue             _renderQueue,
                                        _jobQueue;
                std::thread             _renderThread;
                std::mutex              _renderMutex;
                std::condition_variable _queueReady;
                bool                    _active;

                // Remove stuff that shouldn't work
                //
                impl &operator =( const impl &other ) = delete;
                impl( const impl &other ) = delete;
    };


    Graphics::impl::impl( const Settings &someSettings )
    : _window( 
        sf::VideoMode(
            someSettings.windowSize.x,
            someSettings.windowSize.y ),
        someSettings.windowCaption,
        someSettings.fullscreen ? sf::Style::Fullscreen : sf::Style::Default,
        createContextSettings( someSettings.antialiasLevel ) )
    , _active( true )
    {
        init();
    }


    Graphics::impl::impl( const Settings &someSettings, const HWND aWindowHandle )
    : _window(
        aWindowHandle,
        createContextSettings( someSettings.antialiasLevel ) )
    , _active( true )
    {
        init();
    }


    Graphics::impl::~impl( void )
    {
        if( hasRenderThread() )
        {
            _queueReady.notify_all();

            if( _renderThread.joinable() )
            {
                _renderThread.join();
            }
        }
    }


    void Graphics::impl::update( void )
    {
        sf::Event event;
        while( _window.pollEvent( event ) )
        {
            if( event.type == sf::Event::Closed )
            {
                _window.close();
            }
        }
    }


    void Graphics::impl::render( const SimulationCUPtr &aSimulation )
    {
        RenderQueue &jobQueue = hasRenderThread() ? _jobQueue : _renderQueue;

        aSimulation->visitEntities( [this, &jobQueue]( const SimulationEntityUPtr &anEntity )
        {
            auto &&foundDrawableIterator = _drawables.find( anEntity->id() );

            if( foundDrawableIterator != _drawables.end() )
            {
                IDrawableUPtr &foundDrawable = foundDrawableIterator->second;
                jobQueue.push_back( foundDrawable->createRenderJob( anEntity->position() ) );
            }
        } );

        if( hasRenderThread() )
        {
            {
                std::unique_lock<std::mutex> lock( _renderMutex );
                std::swap( jobQueue, _renderQueue );
            }
            _queueReady.notify_all();
        }
        else
        {
            renderQueue( _renderQueue );
        }
    }


    void Graphics::impl::setWindowSize( const unsigned int aWidth, const unsigned int aHeight )
    {
        sf::FloatRect viewRect;
        viewRect.left = 0;
        viewRect.top = 0;
        viewRect.width = static_cast<float>( aWidth );
        viewRect.height = static_cast<float>( aHeight );
        _window.setView( sf::View( viewRect ) );
    }

    
    void Graphics::impl::finish( void )
    {
        _active = false;
    }


    void Graphics::impl::loadDrawables( std::istream &anInputStream )
    {
        EntityId currentId;

        while( anInputStream >> currentId )
        {
            _drawables.insert( DrawableMap::value_type( currentId, DrawableFactory::CreateDrawable( anInputStream ) ) );
        }
    }


    bool Graphics::impl::isWindowOpen( void ) const
    {
        return _window.isOpen();
    }


    sf::ContextSettings Graphics::impl::createContextSettings( const int anAntialiasLevel )
    {
        sf::ContextSettings settings;
        settings.antialiasingLevel = anAntialiasLevel;

        return settings;
    }


    void Graphics::impl::init( void )
    {
        CircleDrawable::RegisterFactory();
        SpriteDrawable::RegisterFactory();
        _renderThread = std::thread( &Graphics::impl::renderInThread, this );
        _window.setActive( false );
    }


    void Graphics::impl::renderInThread( void )
    {
        while( _window.isOpen() && _active )
        {
            std::unique_lock<std::mutex> lock( _renderMutex );
            _queueReady.wait( lock, [this]{ return !_renderQueue.empty() || !_window.isOpen() || !_active; } );

            if( !_window.isOpen() || !_active )
            {
                return;
            }

            renderQueue( _renderQueue );
        }
    }


    void Graphics::impl::renderQueue( const RenderQueue &aRenderQueue )
    {
            _window.clear();

            for( auto&& currentJob : _renderQueue )
            {
                currentJob->draw( _window );
            }

            _window.display();

            _renderQueue.clear();
    }


    bool Graphics::impl::hasRenderThread( void )
    {
        return _renderThread.get_id() != std::thread::id();
    }


    Graphics::Graphics( const Settings &someSettings )
    : _impl( std::make_unique<impl>( someSettings ) )
    {
    }


    Graphics::Graphics( const Settings &someSettings, const HWND aWindowHandle )
    : _impl( std::make_unique<impl>( someSettings, aWindowHandle ) )
    {
    }


    Graphics::~Graphics( void )
    {
    }


    void Graphics::update( void )
    {
        _impl->update();
    }


    void Graphics::render( const SimulationCUPtr &aSimulation )
    {
        _impl->render( aSimulation );
    }


    void Graphics::setWindowSize( const unsigned int aWidth, const unsigned int aHeight )
    {
        _impl->setWindowSize( aWidth, aHeight );
    }


    void Graphics::finish( void )
    {
        _impl->finish();
    }


    void Graphics::loadDrawables( std::istream &anInputStream )
    {
        _impl->loadDrawables( anInputStream );
    }


    bool Graphics::isWindowOpen( void ) const
    {
        return _impl->isWindowOpen();
    }
}
