#include "stdafx.h"
#include "sprite_drawable.h"

#include "sprite_render_job.h"
#include "sfml_conversions.h"
#include "drawable_factory.h"
#include "texture_cache.h"


namespace mhm
{
    const HashedString SpriteDrawable::Type( "Sprite" );


    SpriteDrawable::SpriteDrawable( const sf::Texture &aTexture )
    : _sprite( aTexture )
    {}


    IRenderJobUPtr SpriteDrawable::createRenderJob( const Vector2f &aPosition )
    {
        return std::make_unique<SpriteRenderJob>( _sprite, ToRenderer( aPosition ) );
    }


    class SpriteDrawableFactory : public DrawableFactory
    {
    public:     SpriteDrawableFactory( void );

    private:    virtual IDrawableUPtr makeDrawable( std::istream &anInputStream );
                virtual const HashedString &type( void ) const;

                sf::Texture _spriteTexture;
    };


    SpriteDrawableFactory::SpriteDrawableFactory( void )
    {
        _spriteTexture.loadFromFile( "pirate.png" );
    }


    IDrawableUPtr SpriteDrawableFactory::makeDrawable( std::istream &anInputStream )
    {
        HashedString spriteTextureId;
        anInputStream >> spriteTextureId;

        //sf::Texture &spriteTexture = TextureCache::Instance()->getTexture( spriteTextureId );

        return std::make_unique<SpriteDrawable>( _spriteTexture );
    }


    const HashedString &SpriteDrawableFactory::type( void ) const
    {
        return SpriteDrawable::Type;
    }


    void SpriteDrawable::RegisterFactory( void )
    {
        DrawableFactoryUPtr spriteDrawableFactory = std::make_unique<SpriteDrawableFactory>();

        DrawableFactory::Register( spriteDrawableFactory );
    }
}
