#include "stdafx.h"
#include "simulation_entity.h"


namespace mhm
{
    SimulationEntity::SimulationEntity( const EntityId &anId, const Vector2f &aStartPosition )
    : _id( anId )
    , _position( aStartPosition )
    {

    }
}
