#pragma once



namespace mhm
{
    sf::Color ToRenderer( const Color &aColor );
    sf::Vector2f ToRenderer( const Vector2f &aVector );
}