#include "stdafx.h"
#include "sprite_render_job.h"


namespace mhm
{
    SpriteRenderJob::SpriteRenderJob( const sf::Sprite &aSprite, const sf::Vector2f &aPosition )
    : _sprite( aSprite )
    {
        _sprite.setPosition( aPosition );
    }


    void SpriteRenderJob::draw( sf::RenderWindow &aWindow )
    {
        aWindow.draw( _sprite );
    }
}