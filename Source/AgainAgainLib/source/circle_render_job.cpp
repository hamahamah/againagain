#include "stdafx.h"
#include "circle_render_job.h"


namespace mhm
{
    CircleRenderJob::CircleRenderJob( const sf::CircleShape &aShape, const sf::Vector2f &aPosition )
    : _circle( aShape )
    {
        _circle.setPosition( sf::Vector2f( aPosition.x - aShape.getRadius(), aPosition.y - aShape.getRadius() ) );
    }


    void CircleRenderJob::draw( sf::RenderWindow &aWindow )
    {
        aWindow.draw( _circle );
    }
}
