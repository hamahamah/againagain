#pragma once


// Forward declarations
//
namespace mhm
{
    class IRenderJob;
    typedef std::unique_ptr<IRenderJob> IRenderJobUPtr;
}


namespace mhm
{
    class IDrawable
    {
    public:     virtual IRenderJobUPtr createRenderJob( const Vector2f &aPosition ) = 0;
    };

    typedef std::unique_ptr<IDrawable> IDrawableUPtr;
}