#include "stdafx.h"
#include "drawable_factory.h"

#include "i_drawable.h"


namespace mhm
{
    DrawableFactory::FactoryRegistry DrawableFactory::_Factories;


    void DrawableFactory::Register( DrawableFactoryUPtr &aFactory )
    {
        auto &&foundFactoryIterator = _Factories.find( aFactory->type() );
        if( foundFactoryIterator != _Factories.end() )
        {
            throw std::exception( "Factory for type already registered." );
        }

        _Factories[aFactory->type()].swap( aFactory );
    }


    IDrawableUPtr DrawableFactory::CreateDrawable( std::istream &anInputStream )
    {
        HashedString drawableType;
        anInputStream >> drawableType;

        auto &&foundFactoryIterator = _Factories.find( drawableType );
        if( foundFactoryIterator == _Factories.end() )
        {
            throw std::exception( "Factory for type not found." );
        }

        IDrawableUPtr newDrawable = foundFactoryIterator->second->makeDrawable( anInputStream );

        return newDrawable;
    }
}