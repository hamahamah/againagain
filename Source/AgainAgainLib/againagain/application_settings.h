#pragma once


namespace mhm
{
    struct ApplicationSettings
    {
        // Enums
        //
        enum AntialiasLevel
        {
            None,
            x1,
            x2,
            x4,
            x8,
            NumAntialiasLevels
        };


        enum Resolution
        {
            res640x480,
            res800x600,
            res1024x768,
            NumResolutions
        };

        // Static members
        //
        const static std::array<Vector2i, Resolution::NumResolutions> Resolutions;
        const static std::array<int, AntialiasLevel::NumAntialiasLevels> AntialiasLevels;
        const static std::string TypeHeader;
        const static Version SupportedVersion;

        // Factories
        //
        static ApplicationSettings CreateFromFile( const std::string &aFilename );

        // Operators
        //
        friend std::istream &operator >>( std::istream &anInputFile, ApplicationSettings &someSettings );

        // Members
        //
        Vector2i        windowSize;
        int             antialiasLevel;
        bool            fullscreen;
        std::string     windowCaption;
    };
}
