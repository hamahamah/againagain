#pragma once


namespace mhm
{
    typedef HashedString EntityId;
    typedef HashedString ResourcePackId;
    typedef HashedString ResourceId;
    typedef HashedString TextureId;


    enum PlayerMovementDirection
    {
        Up,
        Right,
        Down,
        Left,
        NumDirections
    };
}
