#pragma once


#include "types.h"


// Forward declarations
//
namespace mhm
{
    struct ApplicationSettings;

    class Application;
    typedef std::unique_ptr<Application> ApplicationUPtr;
}


namespace mhm
{
    class Application
    {
    public:     static void Create( const ApplicationSettings &someSettings );
                static void Create( const ApplicationSettings &someSettings, const HWND aWindowHandle );
                static Application *Instance( void );
                static void Destroy( void );

                ~Application( void );

                void start( void );
                bool update( void );
                void setPlayerMovement( const PlayerMovementDirection aDirection, const bool aMoveFlag );
                void setWindowSize( const unsigned int aWidth, const unsigned int aHeight );
                void disableInput( void );
                void enableInput( void );

                // States
                //
                //void pushState( ApplicationState *aState );
                //void popState( void );

    private:    class impl;
                std::unique_ptr<impl> _impl;

                Application( const ApplicationSettings &someSettings );
                Application( const ApplicationSettings &someSettings, const HWND aWindowHandle );

                static ApplicationUPtr _Instance;
    };
}
