#pragma once


// Forward declarations
//
namespace mhm
{
    template<typename TYPE>
    class Vector4;
}


//
// Vector3
//

namespace mhm
{
    template<typename TYPE>
    class Vector3
    {
    public:		// Construction
                //
                Vector3( void );
                Vector3( const Vector3 &aVector3 );
                Vector3( Vector3 &&aVector3 );
                explicit Vector3( const mhm::Vector4<TYPE> &aVector4 );
                template<typename U> explicit Vector3( const Vector3<U> &aVector );
                Vector3( const TYPE anX, const TYPE anY, const TYPE aZ );

                // Swap
                //
                friend void swap( Vector3 &aFirst, Vector3 &aSecond )
                {
                    using std::swap;

                    swap( aFirst.x, aSecond.x );
                    swap( aFirst.y, aSecond.y );
                    swap( aFirst.z, aSecond.z );
                }


                friend void swap( Vector3 &aFirst, mhm::Vector4<TYPE> &aSecond )
                {
                    using std::swap;

                    swap( aFirst.x, aSecond.x );
                    swap( aFirst.y, aSecond.y );
                    swap( aFirst.z, aSecond.z );
                    aSecond.w = 1;
                }


                // Arithmetic
                //
                friend Vector3 operator +( Vector3 aLeft, const Vector3 &aRight )
                {
                    return aLeft += aRight;
                }


                friend Vector3 operator -( Vector3 aLeft, const Vector3 &aRight )
                {
                    return aLeft -= aRight;
                }


                friend Vector3 operator *( Vector3 aLeft, const TYPE aRight )
                {
                    return aLeft *= aRight;
                }


                friend Vector3 operator /( Vector3 aLeft, const TYPE aRight )
                {
                    return aLeft /= aRight;
                }


                friend Vector3 operator -( Vector3 aRight )
                {
                    aRight.x = -aRight.x;
                    aRight.y = -aRight.y;
                    aRight.z = -aRight.z;

                    return aRight;
                }

                // Comparison
                //
                friend bool operator ==( const Vector3 &aLeft, const Vector3 &aRight )
                {
                    return ( aLeft.x == aRight.x && aLeft.y == aRight.y, aLeft.z == aLeft.z );
                }


                friend bool operator !=( const Vector3 &aLeft, const Vector3 &aRight )
                {
                    return !( aLeft == aRight );
                }

                // Assignment
                //
                Vector3 &operator =( Vector3 aRight );
                Vector3 &operator =( Vector4<TYPE> aRight );

                // Combined
                //
                Vector3 &operator +=( const Vector3 &aRight );
                Vector3 &operator -=( const Vector3 &aRight );
                Vector3 &operator *=( const TYPE aRight );
                Vector3 &operator /=( const TYPE aRight );

                // Suffix
                //
                TYPE &operator []( const unsigned int anIndex );
                TYPE operator []( const unsigned int anIndex ) const;

                // Info
                //
                TYPE length( void ) const;
                TYPE squareLength( void ) const;
                TYPE manhattan( const Vector3 &aVector ) const;

                // Manipulation
                //
                //const TYPE Angle( void ) const;
                //void Rotate( const TYPE anAngle );
                //Vector3 Normal( void ) const;
                Vector3 &normalize( void );

#pragma warning( suppress : 4201 ) // NOTE: This works fine in VC2013 and GCC so I'm keeping it. //MAHAM
                union
                {
                    struct
                    {
                        union
                        {
                            TYPE    x,
                                u;
                        };

                        union
                        {
                            TYPE    y,
                                v;
                        };

                        union
                        {
                            TYPE    z,
                                w;
                        };
                    };

                    std::array<TYPE, 3> components;
                };

                static TYPE Dot( const Vector3 &aFirstVector, const Vector3 &aSecondVector );
                static Vector3 Cross( const Vector3 &aFirstVector, const Vector3 &aSecondVector );
                static Vector3 Normalize( const Vector3 &aVector );

                static const Vector3	Zero,
                                        UnitX,
                                        UnitY,
                                        UnitZ,
                                        One;
    };

    typedef Vector3<char>   Vector3c, Point3c;
    typedef Vector3<float>  Vector3f, Point3f;
    typedef Vector3<int>    Vector3i, Point3i;
    typedef Vector3<double> Vector3d, Point3d;

    template<typename TYPE> const Vector3<TYPE> Vector3<TYPE>::Zero( 0, 0, 0 );
    template<typename TYPE> const Vector3<TYPE> Vector3<TYPE>::UnitX( 1, 0, 0 );
    template<typename TYPE> const Vector3<TYPE> Vector3<TYPE>::UnitY( 0, 1, 0 );
    template<typename TYPE> const Vector3<TYPE> Vector3<TYPE>::UnitZ( 0, 0, 1 );
    template<typename TYPE> const Vector3<TYPE> Vector3<TYPE>::One( 1, 1, 1 );
}

#include "vector3.inl"
