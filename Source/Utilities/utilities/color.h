#pragma once


namespace mhm
{
    /**
     *  Color
     */
    class Color
    {
    public:     /**
                 *  Default constructor. Sets all components to 0.
                 */
                Color( void );

                /**
                 *  Copy constructor. Copies all values of input color.
                 */
                Color( const Color &other );

                /**
                 *  This constructor takes each component value.
                 *
                 *  @param anR gives the amount of red in the color.
                 *  @param aG gives the amount of green in the color.
                 *  @param aB gives the amount of blue in the color.
                 *  @param anA gives the alpha value of the color.
                 */
                Color( const float anR, const float aG, const float aB, const float anA );

                friend std::istream &operator >>( std::istream &anInputStream, Color &aColor );
                friend std::ostream &operator <<( std::ostream &anOutputStream, Color &aColor );

// NOTE: This works fine in VC2013 and GCC so I'm keeping it. //MAHAM
#pragma warning( push )
#pragma warning( disable : 4201 )
                union
                {
                    struct
                    {
                        float   r,
                                g,
                                b,
                                a;
                    };

                    std::array<float, 4> components;
                };
#pragma warning( pop )

                // Predefined primary colors.
                //
                static const Color  Red,
                                    Green,
                                    Blue,
                                    Cyan,
                                    Magenta,
                                    Yellow,
                                    Black,
                                    White,
                                    Transparent;
            };
}