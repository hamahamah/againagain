#pragma once


namespace mhm
{
    template<typename TYPE>
    class Vector2
    {
    public:		// Construction
                //
                Vector2( void );
                Vector2( const Vector2 &aVector2 );
                Vector2( Vector2 &&aVector2 );
                template<typename U> explicit Vector2( const Vector2<U> &aVector2 );
                Vector2( const TYPE anX, const TYPE anY );

                // Swap
                //
                friend void swap( Vector2 &aFirst, Vector2 &aSecond )
                {
                    using std::swap;

                    swap( aFirst.x, aSecond.x );
                    swap( aFirst.y, aSecond.y );
                }

                // Arithmetic
                //
                friend Vector2 operator +( Vector2 aLeft, const Vector2 &aRight )
                {
                    return aLeft += aRight;
                }


                friend Vector2 operator -( Vector2 aLeft, const Vector2 &aRight )
                {
                    return aLeft -= aRight;
                }


                friend Vector2 operator *( Vector2 aLeft, const TYPE aRight )
                {
                    return aLeft *= aRight;
                }


                friend Vector2 operator /( Vector2 aLeft, const TYPE aRight )
                {
                    return aLeft /= aRight;
                }


                friend Vector2 operator -( Vector2 aRight )
                {
                    aRight.x = -aRight.x;
                    aRight.y = -aRight.y;

                    return aRight;
                }

                // Comparison
                //
                friend bool operator ==( const Vector2 &aLeft, const Vector2 &aRight )
                {
                    return ( aLeft.x == aRight.x && aLeft.y == aRight.y );
                }


                friend bool operator !=( const Vector2 &aLeft, const Vector2 &aRight )
                {
                    return !( aLeft == aRight );
                }

                // Assignment
                //
                Vector2 &operator =( Vector2 aRight );

                // Compound assignment
                //
                Vector2 &operator +=( const Vector2 &aRight );
                Vector2 &operator -=( const Vector2 &aRight );
                Vector2 &operator *=( const TYPE aRight );
                Vector2 &operator /=( const TYPE aRight );

                // Suffix
                //
                TYPE &operator []( const unsigned int anIndex );
                TYPE operator []( const unsigned int anIndex ) const;

                // Info
                //
                TYPE length( void ) const;
                TYPE squareLength( void ) const;
                TYPE angle( void ) const;
                Vector2 normal( void ) const;
                TYPE manhattan( const Vector2 &aVector ) const;

                // Manipulation
                //
                Vector2 &rotate( const TYPE anAngle );
                Vector2 &normalize( void );

#pragma warning( suppress : 4201 ) // NOTE: This works fine in VC2013 and GCC so I'm keeping it. //MAHAM
                union
                {
                    struct
                    {
                        union
                        {
                            TYPE    x,
                                    u;
                        };

                        union
                        {
                            TYPE    y,
                                    w;
                        };
                    };

                    std::array<TYPE, 2> components;
                };

                static TYPE Dot( const Vector2 &aFirstVector, const Vector2 &aSecondVector );
                static Vector2 Normalize( const Vector2 &aVector );

                static const Vector2	Zero,
                                        UnitX,
                                        UnitY,
                                        One;
    };

    typedef Vector2<char>   Vector2c, Point2c;
    typedef Vector2<float>  Vector2f, Point2f;
    typedef Vector2<int>    Vector2i, Point2i;
    typedef Vector2<double> Vector2d, Point2d;

    template<typename TYPE> const Vector2<TYPE> Vector2<TYPE>::Zero( 0, 0 );
    template<typename TYPE> const Vector2<TYPE> Vector2<TYPE>::UnitX( 1, 0 );
    template<typename TYPE> const Vector2<TYPE> Vector2<TYPE>::UnitY( 0, 1 );
    template<typename TYPE> const Vector2<TYPE> Vector2<TYPE>::One( 1, 1 );
}

#include "vector2.inl"
