#include "vector4.h"

//
// Construction
//


template<typename TYPE>
inline mhm::Vector3<TYPE>::Vector3( void )
: x( 0 )
, y( 0 )
, z( 0 )
{
}


template<typename TYPE>
inline mhm::Vector3<TYPE>::Vector3( const Vector3 &anOther )
: x( anOther.x )
, y( anOther.y )
, z( anOther.z )
{
}


template<typename TYPE>
inline mhm::Vector3<TYPE>::Vector3( Vector3 &&anOther )
: x( std::move( anOther.x ) )
, y( std::move( anOther.y ) )
, z( std::move( anOther.z ) )
{
}


template<typename TYPE>
inline mhm::Vector3<TYPE>::Vector3( const mhm::Vector4<TYPE> &anOther )
: x( anOther.x )
, y( anOther.y )
, z( anOther.z )
{
}


template<typename TYPE>
template<typename U>
inline mhm::Vector3<TYPE>::Vector3( const Vector3<U> &aVector )
: x(  static_cast<TYPE>( aVector.x ) )
, y(  static_cast<TYPE>( aVector.y ) )
, z(  static_cast<TYPE>( aVector.z ) )
{
}


template<typename TYPE>
inline mhm::Vector3<TYPE>::Vector3( const TYPE anX, const TYPE anY, const TYPE aZ )
: x( anX )
, y( anY )
, z( aZ )
{
}


//
// Assignment
//

template<typename TYPE>
inline mhm::Vector3<TYPE> &mhm::Vector3<TYPE>::operator =( Vector3<TYPE> aRight )
{
    using std::swap;
    swap( *this, aRight );

    return *this;
}


template<typename TYPE>
inline mhm::Vector3<TYPE> &mhm::Vector3<TYPE>::operator =( mhm::Vector4<TYPE> aRight )
{
    using std::swap;
    swap( *this, aRight );

    return *this;
}


//
// Combined
//


template<typename TYPE>
inline mhm::Vector3<TYPE> &mhm::Vector3<TYPE>::operator +=( const Vector3<TYPE> &aVector )
{
    x += aVector.x;
    y += aVector.y;
    z += aVector.z;

    return *this;
}


template<typename TYPE>
inline mhm::Vector3<TYPE> &mhm::Vector3<TYPE>::operator -=( const Vector3<TYPE> &aVector )
{
    x -= aVector.x;
    y -= aVector.y;
    z -= aVector.z;

    return *this;
}


template<typename TYPE>
inline mhm::Vector3<TYPE> &mhm::Vector3<TYPE>::operator *=( const TYPE aMagnitude )
{
    x *= aMagnitude;
    y *= aMagnitude;
    z *= aMagnitude;

    return *this;
}


template<typename TYPE>
inline mhm::Vector3<TYPE> &mhm::Vector3<TYPE>::operator /=( const TYPE aDivisor )
{
    x /= aDivisor;
    y /= aDivisor;
    z /= aDivisor;

    return *this;
}


//
// Suffix
//


template<typename TYPE>
inline TYPE &mhm::Vector3<TYPE>::operator []( const unsigned int anIndex )
{
    return components[anIndex];
}


template<typename TYPE>
inline TYPE mhm::Vector3<TYPE>::operator []( const unsigned int anIndex ) const
{
    return components[anIndex];
}


//
// Info
//


template<typename TYPE>
inline TYPE mhm::Vector3<TYPE>::length( void ) const
{
    return sqrt( x * x + y * y + z * z );
}


template<typename TYPE>
inline TYPE mhm::Vector3<TYPE>::squareLength( void ) const
{
    return x * x + y * y + z * z;
}


template<typename TYPE>
inline TYPE mhm::Vector3<TYPE>::manhattan( const Vector3<TYPE> &aVector ) const
{
    return std::abs( x - aVector.x ) + std::abs( y - aVector.y ) + std::abs( z - aVector.z );
}


//
// Manipulation
//

//const TYPE Vector3::Angle( void ) const
//{
//	return atan2( y, x );
//}
//
//
//void Vector3::Rotate( const TYPE anAngle )
//{
//	TYPE length = Length();
//
//	x = length * cos( anAngle );
//	y = length * sin( anAngle );
//}
//
//
//Vector3 Vector3::Normal( void ) const
//{
//	return Vector3( y, x );
//}


template<typename TYPE>
inline mhm::Vector3<TYPE> &mhm::Vector3<TYPE>::normalize( void )
{
    TYPE vectorLength = length();

    x /= vectorLength;
    y /= vectorLength;
    z /= vectorLength;

    return *this;
}


//
// Static members
//


template<typename TYPE>
inline TYPE mhm::Vector3<TYPE>::Dot( const Vector3<TYPE> &aFirstVector, const Vector3<TYPE> &aSecondVector )
{
    return (
          aFirstVector.x * aSecondVector.x
        + aFirstVector.y * aSecondVector.y
        + aFirstVector.z * aSecondVector.z );
};


template<typename TYPE>
inline mhm::Vector3<TYPE> mhm::Vector3<TYPE>::Cross(
    const Vector3<TYPE> &aFirstVector,
    const Vector3<TYPE> &aSecondVector )
{
    Vector3<TYPE> crossProduct;

    crossProduct.x = aFirstVector.y * aSecondVector.z - aFirstVector.z * aSecondVector.y;
    crossProduct.y = aFirstVector.z * aSecondVector.x - aFirstVector.x * aSecondVector.z;
    crossProduct.z = aFirstVector.x * aSecondVector.y - aFirstVector.y * aSecondVector.x;

    return crossProduct;
}


template<typename TYPE>
inline mhm::Vector3<TYPE> mhm::Vector3<TYPE>::Normalize( const Vector3<TYPE> &aVector )
{
    Vector3<TYPE> copy( aVector );

    return copy.normalize();
}

