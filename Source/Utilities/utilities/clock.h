#pragma once


#include "time.h"


namespace mhm
{
    class Clock
    {
    public:     virtual Time time( void ) const = 0;
                virtual void update( void ) = 0;
    };
}
