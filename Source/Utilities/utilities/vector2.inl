#include "vector3.h"




//
// Construction
//


template<typename TYPE>
inline mhm::Vector2<TYPE>::Vector2( void )
: x( 0 )
, y( 0 )
{
}


template<typename TYPE>
inline mhm::Vector2<TYPE>::Vector2( const Vector2<TYPE> &anOther )
: x( anOther.x )
, y( anOther.y )
{
}


template<typename TYPE>
inline mhm::Vector2<TYPE>::Vector2( Vector2<TYPE>&& anOther )
: x( std::move( anOther.x ) )
, y( std::move( anOther.y ) )
{
}


template<typename TYPE>
template<typename U>
inline mhm::Vector2<TYPE>::Vector2( const Vector2<U> &aVector )
: x( static_cast<TYPE>( aVector.x ) )
, y( static_cast<TYPE>( aVector.y ) )
{
}


template<typename TYPE>
inline mhm::Vector2<TYPE>::Vector2( const TYPE anX, const TYPE anY )
: x( anX )
, y( anY )
{
}


//
// Assignment
//


template<typename TYPE>
inline mhm::Vector2<TYPE> &mhm::Vector2<TYPE>::operator =( Vector2<TYPE> aRight )
{
    using std::swap;
    swap( *this, aRight );

    return *this;
}


//
// Combined
//


template<typename TYPE>
inline mhm::Vector2<TYPE> &mhm::Vector2<TYPE>::operator +=( const Vector2<TYPE> &aRight )
{
    x += aRight.x;
    y += aRight.y;

    return *this;
}


template<typename TYPE>
inline mhm::Vector2<TYPE> &mhm::Vector2<TYPE>::operator -=( const Vector2<TYPE> &aRight )
{
    x -= aRight.x;
    y -= aRight.y;

    return *this;
}


template<typename TYPE>
inline mhm::Vector2<TYPE> &mhm::Vector2<TYPE>::operator *=( const TYPE aRight )
{
    x *= aRight;
    y *= aRight;

    return *this;
}


template<typename TYPE>
inline mhm::Vector2<TYPE> &mhm::Vector2<TYPE>::operator /=( const TYPE aRight )
{
    x /= aRight;
    y /= aRight;

    return *this;
}


//
// Suffix
//


template<typename TYPE>
inline TYPE &mhm::Vector2<TYPE>::operator []( const unsigned int anIndex )
{
    return components[anIndex];
}


template<typename TYPE>
inline TYPE mhm::Vector2<TYPE>::operator []( const unsigned int anIndex ) const
{
    return components[anIndex];
}


//
// Info
//


template<typename TYPE>
inline TYPE mhm::Vector2<TYPE>::length( void ) const
{
    return sqrt( x * x + y * y );
}


template<typename TYPE>
inline TYPE mhm::Vector2<TYPE>::squareLength( void ) const
{
    return x * x + y * y;
}


template<typename TYPE>
inline TYPE mhm::Vector2<TYPE>::angle( void ) const
{
    return atan2( y, x );
}


template<typename TYPE>
inline mhm::Vector2<TYPE> mhm::Vector2<TYPE>::normal( void ) const
{
    return Vector2( y, -x );
}


template<typename TYPE>
inline TYPE mhm::Vector2<TYPE>::manhattan( const Vector2<TYPE> &aVector ) const
{
    return std::abs( x - aVector.x ) + std::abs( y - aVector.y );
}


//
// Manipulation
//


template<typename TYPE>
inline mhm::Vector2<TYPE> &mhm::Vector2<TYPE>::rotate( const TYPE anAngle )
{
    TYPE    tempX = x,
            tempY = y;
    x = std::cos( anAngle ) * tempX - std::sin( anAngle ) * tempY;
    y = std::sin( anAngle ) * tempX + std::cos( anAngle ) * tempY;

    return *this;
}


template<typename TYPE>
inline mhm::Vector2<TYPE> &mhm::Vector2<TYPE>::normalize( void )
{
    TYPE vectorLength = length();

    x /= vectorLength;
    y /= vectorLength;

    return *this;
}


//
// Static members
//


template<typename TYPE>
inline TYPE mhm::Vector2<TYPE>::Dot( const Vector2<TYPE> &aFirstVector, const Vector2<TYPE> &aSecondVector )
{
    return ( aFirstVector.x * aSecondVector.x + aFirstVector.y * aSecondVector.y );
};


template<typename TYPE>
inline mhm::Vector2<TYPE> mhm::Vector2<TYPE>::Normalize( const Vector2<TYPE> &aVector )
{
    Vector2<TYPE> copy( aVector );
    copy.normalize();

    return copy;
}
