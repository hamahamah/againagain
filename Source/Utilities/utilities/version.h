#pragma once


namespace mhm
{
    struct Version
    {

        // Members
        //
#pragma warning( push )
#pragma warning( disable : 4201 ) // NOTE: This works fine in VC2013 and GCC so I'm keeping it. //MAHAM
        union
        {
            friend std::istream &operator >>( std::istream &anInput, Version &aVersion );

            struct
            {
                int major,
                    minor,
                    patch;
            };

            std::array<int, 3> components;
        };
#pragma warning( pop )
    };
}
