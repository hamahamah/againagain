#pragma once


namespace mhm
{
    struct HashedString
    {
        typedef size_t HashType;
        typedef std::string IdentifierType;

        HashedString( void );
        HashedString( const IdentifierType &anIdentifier );
        HashedString( const HashedString &other );

        HashedString &operator =( const HashedString &aLeft );
        friend bool operator ==( const HashedString &aRight, const HashedString &aLeft ) { return aRight.hash == aLeft.hash; }
        friend bool operator !=( const HashedString &aRight, const HashedString &aLeft ) { return !( aRight == aLeft ); }
        friend std::istream &operator >>( std::istream &aRight, HashedString &aLeft );

        static std::hash<IdentifierType> HashFunction;
        static const HashedString Base;

        IdentifierType    string;
        HashType          hash;
    };
}


namespace std
{
    template<>
    struct hash<mhm::HashedString>
    {
        typedef mhm::HashedString argument_type;
        typedef std::size_t result_type;

        result_type operator()( argument_type const& s ) const
        {
            return s.hash;
        }
    };
}