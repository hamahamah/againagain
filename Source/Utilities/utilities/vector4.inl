#include "vector3.h"


//
// Construction
//


template<typename TYPE>
inline mhm::Vector4<TYPE>::Vector4( void )
: x( 0 )
, y( 0 )
, z( 0 )
, w( 0 )
{
}


template<typename TYPE>
inline mhm::Vector4<TYPE>::Vector4( const Vector4 &anOther )
: x( anOther.x )
, y( anOther.y )
, z( anOther.z )
, w( anOther.w )
{
}


template<typename TYPE>
inline mhm::Vector4<TYPE>::Vector4( Vector4 &&anOther )
: x( std::move( anOther.x ) )
, y( std::move( anOther.y ) )
, z( std::move( anOther.z ) )
, w( std::move( anOther.w ) )
{
}


template<typename TYPE>
inline mhm::Vector4<TYPE>::Vector4( const mhm::Vector3<TYPE> &anOther )
: x( anOther.x )
, y( anOther.y )
, z( anOther.z )
, w( 1 )
{
}


template<typename TYPE>
template<typename U>
inline mhm::Vector4<TYPE>::Vector4( const Vector4<U> &anOther )
: x( static_cast<TYPE>( anOther.x ) )
, y( static_cast<TYPE>( anOther.y ) )
, z( static_cast<TYPE>( anOther.z ) )
, w( static_cast<TYPE>( anOther.w ) )
{
}


template<typename TYPE>
inline mhm::Vector4<TYPE>::Vector4( const TYPE anX, const TYPE anY, const TYPE aZ, const TYPE aW )
: x( anX )
, y( anY )
, z( aZ )
, z( aW )
{
}


//
// Comparison
//


template<typename TYPE>
inline bool mhm::Vector4<TYPE>::operator ==( const Vector4<TYPE> &aRight ) const
{
    return ( x == aRight.x && y == aRight.y && z == aRight.z && w == aRight.w );
}


template<typename TYPE>
inline bool mhm::Vector4<TYPE>::operator !=( const Vector4<TYPE> &aRight ) const
{
    return !( *this == aRight );
}


//
// Assignment
//


template<typename TYPE>
inline mhm::Vector4<TYPE> &mhm::Vector4<TYPE>::operator =( Vector4<TYPE> aRight )
{
    using std::swap;
    swap( *this, aRight );

    return *this;
}


template<typename TYPE>
inline mhm::Vector4<TYPE> &mhm::Vector4<TYPE>::operator =( mhm::Vector3<TYPE> aRight )
{
    using std::swap;
    swap( *this, aRight );

    return *this;
}


//
// Combined
//


template<typename TYPE>
inline mhm::Vector4<TYPE> &mhm::Vector4<TYPE>::operator +=( const Vector4<TYPE> &aRight )
{
    x += aRight.x;
    y += aRight.y;
    z += aRight.z;
    w += aRight.w;

    return *this;
}


template<typename TYPE>
inline mhm::Vector4<TYPE> &mhm::Vector4<TYPE>::operator -=( const Vector4 &aRight )
{
    x -= aRight.x;
    y -= aRight.y;
    z -= aRight.z;
    w -= aRight.w;

    return *this;
}


template<typename TYPE>
inline mhm::Vector4<TYPE> &mhm::Vector4<TYPE>::operator *=( const TYPE aRight )
{
    x *= aRight;
    y *= aRight;
    z *= aRight;
    w *= aRight;

    return *this;
}


template<typename TYPE>
inline mhm::Vector4<TYPE> &mhm::Vector4<TYPE>::operator /=( const TYPE aRight )
{
    x /= aRight;
    y /= aRight;
    z /= aRight;
    w /= aRight;

    return *this;
}


//
// Suffix
//


template<typename TYPE>
inline TYPE &mhm::Vector4<TYPE>::operator []( const unsigned int anIndex )
{
    return components[anIndex];
}


template<typename TYPE>
inline TYPE mhm::Vector4<TYPE>::operator []( const unsigned int anIndex ) const
{
    return components[anIndex];
}


//
// Info
//


template<typename TYPE>
inline TYPE mhm::Vector4<TYPE>::length( void ) const
{
    return sqrt( x * x + y * y + z * z + w * w );
}


template<typename TYPE>
inline TYPE mhm::Vector4<TYPE>::squareLength( void ) const
{
    return x * x + y * y + z * z + w * w;
}

/*
//const TYPE Angle( void ) const;
//void Rotate( const TYPE anAngle );
//Vector4 Normal( void ) const;
*/

template<typename TYPE>
inline mhm::Vector4<TYPE> &mhm::Vector4<TYPE>::normalize( void )
{
    TYPE vectorLength = length();

    x /= vectorLength;
    y /= vectorLength;
    z /= vectorLength;
    w /= vectorLength;

    return *this;
}
