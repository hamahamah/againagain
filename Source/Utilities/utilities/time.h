#pragma once


namespace mhm
{
    class Time
    {
    public:     Time( void );

                long long microseconds( void ) const;
                int milliseconds( void ) const;
                float seconds( void ) const;

                Time &operator +=( const Time &aRight );
                Time &operator -=( const Time &aRight );
                Time &operator *=( const float aRight );
                Time &operator /=( const float aRight );

                static Time Microseconds( const long long anAmount );
                static Time Milliseconds( const int anAmount );
                static Time Seconds( const float anAmount );

    private:    explicit Time( const long long aMicroseconds );
        
                long long _microseconds;
    };


    Time &operator +( Time aLeft, const Time &aRight );
    Time &operator -( Time aLeft, const Time &aRight );
    Time &operator *( Time aLeft, const float aRight );
    Time &operator /( Time aLeft, const float aRight );
}
