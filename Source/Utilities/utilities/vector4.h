#pragma once


// Forward declarations
//
namespace mhm
{
    template<typename TYPE>
    class Vector3;
}


namespace mhm
{
    template<typename TYPE>
    class Vector4
    {
    public:		// Construction
                //
                Vector4( void );
                Vector4( const Vector4 &anOther );
                Vector4( Vector4 &&anOther );
                explicit Vector4( const mhm::Vector3<TYPE> &anOther );
                template<typename U> explicit Vector4( const Vector4<U> &anOther );
                Vector4( const TYPE anX, const TYPE anY, const TYPE aZ, const TYPE aW );

                // Swap
                //
                friend void swap( Vector4 &aFirst, Vector4 &aSecond )
                {
                    using std::swap;

                    swap( aFirst.x, aSecond.x );
                    swap( aFirst.y, aSecond.y );
                    swap( aFirst.z, aSecond.z );
                    swap( aFirst.w, aSecond.w );
                }


                friend void swap( Vector4 &aFirst, mhm::Vector3<TYPE> &aSecond )
                {
                    using std::swap;

                    swap( aFirst.x, aSecond.x );
                    swap( aFirst.y, aSecond.y );
                    swap( aFirst.z, aSecond.z );
                    aFirst.w = 1;
                }


                // Arithmetic
                //
                friend Vector4 operator +( Vector4 aLeft, const Vector4 &aRight )
                {
                    return aLeft += aRight;
                }


                friend Vector4 operator -( Vector4 aLeft, const Vector4 &aRight )
                {
                    return aLeft -= aRight;
                }


                friend Vector4 operator *( Vector4 aLeft, const TYPE aRight )
                {
                    return aLeft *= aRight;
                }


                friend Vector4 operator /( Vector4 aLeft, const TYPE aRight )
                {
                    return aLeft /= aRight;
                }


                friend Vector4 operator -( Vector4 aRight )
                {
                    aRight.x = -aRight.x;
                    aRight.y = -aRight.y;
                    aRight.z = -aRight.z;
                    aRight.w = -aRight.w;

                    return aRight;
                }

                // Comparison
                //
                bool operator ==( const Vector4 &aRight ) const;
                bool operator !=( const Vector4 &aRight ) const;

                // Assignment
                //
                Vector4 &operator =( Vector4 aRight );
                Vector4 &operator =( Vector3<TYPE> aRight );

                // Combined
                //
                Vector4 &operator +=( const Vector4 &aRight );
                Vector4 &operator -=( const Vector4 &aRight );
                Vector4 &operator *=( const TYPE aRight );
                Vector4 &operator /=( const TYPE aRight );

                // Suffix
                //
                TYPE &operator []( const unsigned int anIndex );
                TYPE operator []( const unsigned int anIndex ) const;

                // Info
                //
                TYPE length( void ) const;
                TYPE squareLength( void ) const;
                //TYPE manhattan( const Vector4 &aVector ) const;

                // Manipulation
                //
                //const TYPE Angle( void ) const;
                //void Rotate( const TYPE anAngle );
                //Vector4 Normal( void ) const;
                Vector4 &normalize( void );

#pragma warning( suppress : 4201 ) // NOTE: This works fine in VC2013 and GCC so I'm keeping it. //MAHAM
                union
                {
                    struct
                    {
                        union
                        {
                            TYPE    x,
                                r;
                        };

                        union
                        {
                            TYPE    y,
                                g;
                        };

                        union
                        {
                            TYPE    z,
                                b;
                        };

                        union
                        {
                            TYPE    w,
                                a;
                        };
                    };

                    std::array<TYPE, 4> components;
                };

                static TYPE Dot( const Vector4 &aFirstVector, const Vector4 &aSecondVector );
                static Vector4 Cross( const Vector4 &aFirstVector, const Vector4 &aSecondVector );
                static Vector4 Normalize( const Vector4 &aVector );

                static const Vector4	Zero,
                                        UnitX,
                                        UnitY,
                                        UnitZ,
                                        UnitW,
                                        One;
    };

    typedef Vector4<char>   Vector4c, Point4c;
    typedef Vector4<float>  Vector4f, Point4f;
    typedef Vector4<int>    Vector4i, Point4i;
    typedef Vector4<double> Vector4d, Point4d;

    template<typename TYPE> const Vector4<TYPE> Vector4<TYPE>::Zero( 0, 0, 0, 0 );
    template<typename TYPE> const Vector4<TYPE> Vector4<TYPE>::UnitX( 1, 0, 0, 0 );
    template<typename TYPE> const Vector4<TYPE> Vector4<TYPE>::UnitY( 0, 1, 0, 0 );
    template<typename TYPE> const Vector4<TYPE> Vector4<TYPE>::UnitZ( 0, 0, 1, 0 );
    template<typename TYPE> const Vector4<TYPE> Vector4<TYPE>::UnitW( 0, 0, 0, 1 );
    template<typename TYPE> const Vector4<TYPE> Vector4<TYPE>::One( 1, 1, 1, 1 );
}

#include "vector4.inl"
