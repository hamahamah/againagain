#pragma once


#include "exception.h"


namespace mhm
{
    template< typename TYPE >
    class PluggableFactory
    {
    public:		class Exception : public mhm::Exception
                {
                public:		Exception( const std::string &someDetails ) : mhm::Exception( someDetails ) {}
                };


                typedef std::map< unsigned long, PluggableFactory< TYPE > * > FactoryMap;


                PluggableFactory( const HashedString &anIdentifier )
                {
                    FactoryMap::iterator currentFactory = _GetFactoryRegistry().find( anIdentifier.hash );

                    if( currentFactory != _GetFactoryRegistry().end() )
                    {
                        throw Exception( "Hash for type already used." );
                    }

                    _GetFactoryRegistry()[anIdentifier.hash] = this;
                }


                static TYPE ConstructObject( std::istream &someData )
                {
                    unsigned long type;
                    someData.read( reinterpret_cast<char *>( &type ), sizeof( type ) );
                    FactoryMap::iterator factoryIterator = _GetFactoryRegistry().find( type );
                    if( factoryIterator != _GetFactoryRegistry().end() )
                    {
                        return factoryIterator->second->makeObject( someData );
                    }

                    throw new Exception( "Couldn't find a factory for the given type." );
                }

    protected:	virtual TYPE makeObject( std::istream &someData ) const = 0;

                static FactoryMap &_GetFactoryRegistry( void )
                {
                    static FactoryMap factoryMap;
                    return factoryMap;
                }
    };
}
