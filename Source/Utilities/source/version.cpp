#include "stdafx.h"
#include "version.h"


namespace mhm
{
    std::istream &operator >>( std::istream &anInput, Version &aVersion )
    {
        anInput.read( reinterpret_cast<char *>( &aVersion.major ), sizeof( aVersion.major ) );
        anInput.read( reinterpret_cast<char *>( &aVersion.minor ), sizeof( aVersion.minor ) );
        anInput.read( reinterpret_cast<char *>( &aVersion.patch ), sizeof( aVersion.patch ) );

        return anInput;
    }
}
