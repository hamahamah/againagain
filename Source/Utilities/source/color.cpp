#include "stdafx.h"
#include "color.h"


namespace mhm
{

    // Definitions for premade colors.
    //
    const Color Color::Red        ( 1.0f, 0.0f, 0.0f, 1.0f );
    const Color Color::Green      ( 0.0f, 1.0f, 0.0f, 1.0f );
    const Color Color::Blue       ( 0.0f, 0.0f, 1.0f, 1.0f );
    const Color Color::Cyan       ( 0.0f, 1.0f, 1.0f, 1.0f );
    const Color Color::Magenta    ( 1.0f, 0.0f, 1.0f, 1.0f );
    const Color Color::Yellow     ( 1.0f, 1.0f, 0.0f, 1.0f );
    const Color Color::Black      ( 0.0f, 0.0f, 0.0f, 1.0f );
    const Color Color::White      ( 1.0f, 1.0f, 1.0f, 1.0f );
    const Color Color::Transparent( 0.0f, 0.0f, 0.0f, 0.0f );


    /************************************************/
    Color::Color( void )
    {
    }


    /************************************************/
    Color::Color( const float anR, const float aG, const float aB, const float anA )
    : r( anR )
    , g( aG )
    , b( aB )
    , a( anA )
    {
    }


    /************************************************/
    Color::Color( const Color &other )
    : r( other.r )
    , g( other.g )
    , b( other.b )
    , a( other.a )
    {
    }


    std::istream &operator >>( std::istream &anInputStream, Color &aColor )
    {
        anInputStream.read( reinterpret_cast<char *>( &aColor.r ), sizeof( aColor.r ) );
        anInputStream.read( reinterpret_cast<char *>( &aColor.g ), sizeof( aColor.g ) );
        anInputStream.read( reinterpret_cast<char *>( &aColor.b ), sizeof( aColor.b ) );
        anInputStream.read( reinterpret_cast<char *>( &aColor.a ), sizeof( aColor.a ) );

        return anInputStream;
    }


    std::ostream &operator <<( std::ostream &anOutputStream, Color &aColor )
    {
        anOutputStream.write( reinterpret_cast<char *>( &aColor.r ), sizeof( aColor.r ) );
        anOutputStream.write( reinterpret_cast<char *>( &aColor.g ), sizeof( aColor.g ) );
        anOutputStream.write( reinterpret_cast<char *>( &aColor.b ), sizeof( aColor.b ) );
        anOutputStream.write( reinterpret_cast<char *>( &aColor.a ), sizeof( aColor.a ) );

        return anOutputStream;
    }
}