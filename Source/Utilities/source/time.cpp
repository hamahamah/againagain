#include "stdafx.h"
#include "time.h"


namespace mhm
{
    Time::Time( void )
    : _microseconds( 0 )
    {
    }


    long long Time::microseconds( void ) const
    {
        return _microseconds;
    }


    int Time::milliseconds( void ) const
    {
        return static_cast<int>( _microseconds / 1000 );
    }


    float Time::seconds( void ) const
    {
        return _microseconds / 1000000.0f;
    }


    Time &Time::operator +=( const Time &aRight )
    {
        _microseconds += aRight._microseconds;
        return *this;
    }


    Time &Time::operator -=( const Time &aRight )
    {
        _microseconds -= aRight._microseconds;
        return *this;
    }


    Time &Time::operator *=( const float aRight )
    {
        _microseconds = static_cast<long long>( _microseconds * aRight );
        return *this;
    }


    Time &Time::operator /=( const float aRight )
    {
        _microseconds = static_cast<long long>( _microseconds * aRight );
        return *this;
    }


    Time::Time( const long long aMicroseconds )
    : _microseconds( aMicroseconds )
    {
    }



    Time Time::Microseconds( const long long anAmount )
    {
        return Time( anAmount );
    }


    Time Time::Milliseconds( const int anAmount )
    {
        return Time( anAmount * 1000 );
    }


    Time Time::Seconds( const float anAmount )
    {
        return Time( static_cast<long long>( anAmount * 1000000.0f ) );
    }


    Time &operator +( Time aLeft, const Time &aRight )
    {
        return aLeft += aRight;
    }


    Time &operator -( Time aLeft, const Time &aRight )
    {
        return aLeft -= aRight;
    }


    Time &operator *( Time aLeft, const float aRight )
    {
        return aLeft *= aRight;
    }


    Time &operator /( Time aLeft, const float aRight )
    {
        return aLeft /= aRight;
    }
}
