#include "stdafx.h"
#include "hashed_string.h"


namespace mhm
{
    std::hash<std::string> HashedString::HashFunction;
    const HashedString HashedString::Base( "UNNAMED" );


    HashedString::HashedString( void )
    : string( "UNINITIALIZED" )
    , hash( 0 )
    {
    }


    HashedString::HashedString( const std::string &anIdentifier )
    : string( anIdentifier )
    , hash( HashFunction( anIdentifier ) )
    {
    }


    HashedString::HashedString( const HashedString &other )
    : string( other.string )
    , hash( other.hash )
    {
    }


    std::istream &operator >>( std::istream &aLeft, HashedString &aRight )
    {
        aLeft.read( reinterpret_cast<char *>( &aRight.hash ), sizeof( aRight.hash ) );

        return aLeft;
    }
}
