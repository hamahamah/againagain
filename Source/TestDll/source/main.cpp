#include "stdafx.h"

#include <cmath>
#include <againagain/application_settings.h>
#include <againagain/application.h>


HWND button;


////////////////////////////////////////////////////////////
/// Function called whenever one of our windows receives a message
///
////////////////////////////////////////////////////////////
LRESULT CALLBACK onEvent( HWND handle, UINT message, WPARAM wParam, LPARAM lParam )
{
    switch( message )
    {
        // Quit when we close the main window
    case WM_CLOSE:
    {
        PostQuitMessage( 0 );
        return 0;
    }

    // Quit when we click the "quit" button
    case WM_COMMAND:
    {
        if( reinterpret_cast<HWND>( lParam ) == button )
        {
            PostQuitMessage( 0 );
            return 0;
        }
    }
    }

    return DefWindowProc( handle, message, wParam, lParam );
}


////////////////////////////////////////////////////////////
/// Entry point of application
///
/// \param Instance: Instance of the application
///
/// \return Error code
///
////////////////////////////////////////////////////////////
int main()
{
    HINSTANCE instance = GetModuleHandle( NULL );

    // Define a class for our main window
    WNDCLASS windowClass;
    windowClass.style = 0;
    windowClass.lpfnWndProc = &onEvent;
    windowClass.cbClsExtra = 0;
    windowClass.cbWndExtra = 0;
    windowClass.hInstance = instance;
    windowClass.hIcon = NULL;
    windowClass.hCursor = 0;
    windowClass.hbrBackground = reinterpret_cast<HBRUSH>( COLOR_BACKGROUND );
    windowClass.lpszMenuName = NULL;
    windowClass.lpszClassName = TEXT( "SFML App" );
    RegisterClass( &windowClass );

    // Let's create the main window
    HWND window = CreateWindow( TEXT( "SFML App" ), TEXT( "SFML Win32" ), WS_SYSMENU | WS_VISIBLE, 200, 200, 660, 520, NULL, NULL, instance, NULL );

    // Add a button for exiting
    button = CreateWindow( TEXT( "BUTTON" ), TEXT( "Quit" ), WS_CHILD | WS_VISIBLE, 560, 440, 80, 40, window, NULL, instance, NULL );

    mhm::ApplicationSettings settings = mhm::ApplicationSettings::CreateFromFile( "settings.conf" );
    mhm::Application::Create( settings );

    // Loop until a WM_QUIT message is received
    MSG message;
    message.message = static_cast<UINT>( ~WM_QUIT );
    while( message.message != WM_QUIT )
    {
        if( PeekMessage( &message, NULL, 0, 0, PM_REMOVE ) )
        {
            // If a message was waiting in the message queue, process it
            TranslateMessage( &message );
            DispatchMessage( &message );
        }
        else
        {
            mhm::Application::Instance()->update();
        }
    }

    // Destroy the main window (all its child controls will be destroyed)
    mhm::Application::Destroy();

    DestroyWindow( window );

    // Don't forget to unregister the window class
    UnregisterClass( TEXT( "SFML App" ), instance );

    return EXIT_SUCCESS;
}